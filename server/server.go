package main

import (
	"context"
	"fmt"
	"log"
	"net"

	user "gitlab.com/golang7532607/grpc/proto"
	"google.golang.org/grpc"
)

type server struct {
	user.UnimplementedUserServiceServer
}

func (s *server) GetUser(ctx context.Context, req *user.GetUserRequest) (*user.GetUserResponse, error) {
	// Perform database query or some other logic to retrieve user information
	// For this example, we'll just return some hardcoded data
	u := &user.User{
		Id:    1,
		Name:  "John Doe",
		Email: "john@example.com",
	}
	res := &user.GetUserResponse{User: u}
	return res, nil
}

func (s *server) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.CreateUserResponse, error) {
	// Perform database query or some other logic to create user
	// For this example, we'll just return some hardcoded data
	u := &user.User{
		Id:    3,
		Name:  req.Name,
		Email: req.Email,
	}
	res := &user.CreateUserResponse{User: u}
	return res, nil
}

func main() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", 50051))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	user.RegisterUserServiceServer(s, &server{})
	log.Printf("Starting server on port %d...\n", 50051)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
