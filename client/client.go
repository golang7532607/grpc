package main

import (
	"context"
	"log"

	user "gitlab.com/golang7532607/grpc/proto"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed to connect: %v", err)
	}
	defer conn.Close()
	userClient := user.NewUserServiceClient(conn)

	// Call the GetUser method
	getResponse, err := userClient.GetUser(context.Background(), &user.GetUserRequest{})
	if err != nil {
		log.Fatalf("failed to get user: %v", err)
	}
	log.Printf("User: id=%d name=%s email=%s", getResponse.User.Id, getResponse.User.Name, getResponse.User.Email)

	// Call the CreateUser method
	createResponse, err := userClient.CreateUser(context.Background(), &user.CreateUserRequest{Name: "Bob Smith", Email: "bob@example.com"})
	if err != nil {
		log.Fatalf("failed to create user: %v", err)
	}
	log.Printf("User created: id=%d name=%s email=%s", createResponse.User.Id, createResponse.User.Name, createResponse.User.Email)
}
